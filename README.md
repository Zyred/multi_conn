# 工程简介
Springboot 中并无多个redis同时操作的方法

在本项目中主要是通过配置文件 multi-redis.properties 配置多个客户端

然后 MultiRedisConnProperties 读取所有的配置文件，封装对应的 JavaBean

最后 MultiRedisConnectionFactory 将配置中的所有客户端加载到内存中

##使用方法
```java
@Autowired
private MultiRedisConnectionFactory factory;

@Test
void contextLoads() {
    factory.put("zs", "123456");
    factory.put("ls", "123456");
    factory.put("ll", "123456");
    factory.put("ag", "123456");

    System.out.println(factory.get("zs"));
    System.out.println(factory.get("ls"));
    System.out.println(factory.get("ll"));
    System.out.println(factory.get("ag"));
}
```

## 最终 
在配置文件中配置的不同 database 中保存了运算后的值