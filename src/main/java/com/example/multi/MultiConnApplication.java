package com.example.multi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class MultiConnApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiConnApplication.class, args);
    }

}
