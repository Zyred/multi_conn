package com.example.multi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 *
 * @author zyred
 * @since v 0.1
 **/
@Configuration
public class MultiRedisConnProperties {

    private final String profile = "multi-redis.properties", prefix = "redis.multi.client", command = ".";

    private List<RedisConnProperties> redisConnProperties;

    public MultiRedisConnProperties() {
        try {
            this.redisConnProperties = new ArrayList<>();
            this.loadProfile(new ClassPathResource(profile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void loadProfile(Resource resource) throws IOException {

        if (!resource.exists()) {
            throw new FileNotFoundException("Profile " + profile + " not found.");
        }

        InputStream input = resource.getInputStream();
        Properties p = new Properties();
        p.load(input);

        Set<String> ns = p.stringPropertyNames();
        int len = prefix.length() + 1;

        Set<String> pres = ns.stream().map(a -> a.substring(0, len))
                .collect(Collectors.toSet());

        int size, last = 0;
        if ((size = pres.size()) == 0 || size != (last = calc(size))) {
            throw new MultiException("The number of clients must be 2 to the NTH power, the current number is only "
                    + size +", you can set the number to" + last);
        }

        for (String pre : pres) {
            RedisConnProperties pro = new RedisConnProperties();
            pro.setClientName(pre.substring(pre.lastIndexOf(command) + 1));

            for (String next : ns) {

                String nextPre = next.substring(0, len);
                if (!pre.equals(nextPre)) {continue;}

                int index = next.lastIndexOf(command) + 1;
                String suffix = next.substring(index);

                this.write(pro, suffix, p.getProperty(next));
            }
            this.redisConnProperties.add(pro);
        }

    }

    private void write (Object target, String field, String val) {
        try {
            Class<?> clazz = target.getClass();
            Field f = clazz.getDeclaredField(field);

            f.setAccessible(true);
            Class<?> t = f.getType();

            Object value = null;
            if (t == Integer.class || t == int.class) {
                value = Integer.parseInt(val);
            } else if (t == long.class || t == Long.class) {
                value = Long.parseLong(val);
            }
            f.set(target, value == null ? val : value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public final int getSize () {
        return this.redisConnProperties.size();
    }

    public final List<RedisConnProperties> getMulti () {
        return this.redisConnProperties;
    }

    static final int calc (int size)  {
        int pre = 1, next = pre << 1;
        for (;;) {

            if (size == pre) {
                return size;
            }

            if (size > pre && size < next) {
                return next;
            }

            pre = next;
            next = next << 1;
        }
    }
}
