package com.example.multi.config;

import com.alibaba.fastjson.JSONObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 1. 项目初始化的时候，连接上所有的 redis
 * 2. 将所有的 redis 连接放入到某个数组中进行保存
 * 3. 提供一个 put，get 方法出去
 * </p>
 *
 * @author zyred
 * @since v 0.1
 **/
@Configuration
public class MultiRedisConnectionFactory implements MultiRedisOperation{

    private final MultiRedisConnProperties ps;

    private final Map<Integer, StringRedisTemplate> conns;

    private final int size;


    public MultiRedisConnectionFactory(MultiRedisConnProperties ps) {
        this.ps = ps;
        this.conns = new ConcurrentHashMap<>();
        this.initConn();
        this.size = ps.getSize();
    }

    private void initConn() {
        List<RedisConnProperties> multi = this.ps.getMulti();
        for (int i = 0; i < multi.size(); i++) {
            final RedisConnProperties p = multi.get(i);

            final StringRedisTemplate t = new StringRedisTemplate();

            RedisStandaloneConfiguration sc =
                    new RedisStandaloneConfiguration(p.getHost(), p.getPort());
            sc.setDatabase(p.getDatabase());
            sc.setPassword(p.getPassword());

            JedisConnectionFactory factory = new JedisConnectionFactory(sc);

            t.setConnectionFactory(factory);
            // 必须要调用本方法进行初始化每一个连接
            t.afterPropertiesSet();

            conns.put(i, t);
        }
    }

    @Override
    public void put(String k, Object v) {
        this.putVal(MultiRedisOperation.slot(k, size), k, v);
    }

    @Override
    public void put(String k, Object v, int timeout, TimeUnit unit) {
        this.putVal(MultiRedisOperation.slot(k, size), k, v, timeout, unit);
    }

    void putVal(int hash, String k, Object v) {
        int slot = hash & size - 1;
        StringRedisTemplate temp = this.conns.get(slot);
        temp.opsForValue().set(k, JSONObject.toJSONString(v));
    }

    void putVal(int hash, String k, Object v, int timeout, TimeUnit unit) {
        int slot = hash & size - 1;
        StringRedisTemplate temp = this.conns.get(slot);
        temp.opsForValue().set(k, JSONObject.toJSONString(v), timeout, unit);
    }

    @Override
    public <V> V get (String k, Class<V> clazz) {
        String val = this.conns.get(MultiRedisOperation.slot(k, size)).opsForValue().get(k);
        return JSONObject.parseObject(val, clazz);
    }

    @Override
    public String get (String k) {
        return this.conns.get(MultiRedisOperation.slot(k, size)).opsForValue().get(k);
    }
}
