package com.example.multi.config;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 *     多 redis 连接操作基类
 * </p>
 *
 * @author zyred
 * @since v 0.1
 **/
public interface MultiRedisOperation {

    /**
     * 插入一个不带超时时间的值到 redis 中
     * @param k     K
     * @param v     V
     */
    void put(String k, Object v);

    /**
     * 插入一个带超时时间的值到 redis 中
     *
     * @param k         K
     * @param v         V
     * @param timeout   超时时间
     * @param unit      时间单位
     */
    void put(String k, Object v, int timeout, TimeUnit unit);

    /**
     * 获取一个值
     *
     * @param k     K
     * @param clazz 值类型的class对象
     * @param <V>   类型泛型
     * @return      泛型对应的实体
     */
    <V> V get (String k, Class<V> clazz);

    /**
     * 获取一个 string 类型的值
     *
     * @param k     K
     * @return      string 类型的值
     */
    String get (String k);


    static int slot(String key, int size) {
        int h;
        return ((key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16)) & (size - 1);
    }
}
