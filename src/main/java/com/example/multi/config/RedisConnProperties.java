package com.example.multi.config;

/**
 * <p>
 *
 * </p>
 *
 * @author zyred
 * @since v 0.1
 **/
public class RedisConnProperties {

    /* Redis数据库索引（默认为0） */
    private int database = 0;
    /* Redis服务器地址 */
    private String host = "localhost";
    /* Redis服务器连接端口 */
    private int port = 6379;
    /* Redis服务器连接密码（默认为空） */
    private String password;
    /* 连接超时时间（毫秒） */
    private int timeout;
    /* 客户端名称 */
    private String clientName;
    /* 是否使用 ssl */
    private boolean ssl;
    /* 用户名称 */
    private String username;

    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
