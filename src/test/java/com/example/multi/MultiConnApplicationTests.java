package com.example.multi;

import com.example.multi.config.MultiRedisConnectionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MultiConnApplicationTests {

    @Autowired
    private MultiRedisConnectionFactory factory;

    @Test
    void contextLoads() {
        factory.put("zs", "123456");
        factory.put("ls", "123456");
        factory.put("ll", "123456");
        factory.put("ag", "123456");

        System.out.println(factory.get("zs"));
        System.out.println(factory.get("ls"));
        System.out.println(factory.get("ll"));
        System.out.println(factory.get("ag"));
    }

}
